import sbt._
object Dependencies {

  val scalatest = "org.scalatest" %% "scalatest" % "3.2.0" % "test"
  val scopt = "com.github.scopt" %% "scopt" % "3.7.1"
  val scalaLogging = "ch.qos.logback" % "logback-classic" % "1.2.3"
  val circeVersion = "0.12.3"

  val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)
}
