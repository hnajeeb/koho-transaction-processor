package com.koho.transactionprocessor.process

import java.sql.Timestamp
import java.time.LocalDateTime

import com.koho.transactionprocessor.model.{Response, Transaction}
import com.koho.transactionprocessor.testutil.UnitTest

class ProcessorTest extends UnitTest {
  "A legitimate transaction" should "be performed successfully" in {
    val transaction = Transaction(1234, 1111, 505.50, Timestamp.valueOf(LocalDateTime.now()))
    val processor = new Processor()
    val response: Response = processor.performTransaction(transaction).get

    response.id mustBe transaction.id
    response.customer_id mustBe transaction.customer_id
    response.accepted mustBe true
  }

  "An invalid transaction" should "not be accepted" in {
    val processor = new Processor()
    val transaction = Transaction(1234, 1111, 5001, Timestamp.valueOf(LocalDateTime.now()))
    val response: Response = processor.performTransaction(transaction).get
    response.accepted mustBe false
  }

  "Transactions" should "be accepted till they don't exceed the daily limit" in {
    val processor = new Processor()
    val transactionsToPerform = Seq(
      Transaction(1234, 1111, 3000, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1235, 1111, 3000, Timestamp.valueOf(LocalDateTime.now())),
    )
    val response: Seq[Response] = transactionsToPerform.map(processor.performTransaction).filter(_.isDefined).map(_.get)
    response.map(_.accepted) mustBe Seq(true, false)
  }

  "Daily limit of 3 loads" should "not be exceeded" in {
    val processor = new Processor()
    val transactionsToPerform = Seq(
      Transaction(1234, 1111, 300, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1235, 1111, 300, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1236, 1111, 300, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1237, 1111, 300, Timestamp.valueOf(LocalDateTime.now())),
    )
    val response: Seq[Response] = transactionsToPerform.map(processor.performTransaction).filter(_.isDefined).map(_.get)
    response.map(_.accepted) mustBe Seq(true, true, true, false)
  }

  "Transactions by same user with same loadId" should "be ignored" in {
    val processor = new Processor()
    val transactionsToPerform =  Seq(
      Transaction(1234, 1111, 300, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1234, 1111, 400, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1234, 1111, 500, Timestamp.valueOf(LocalDateTime.now())),
      Transaction(1234, 1111, 600, Timestamp.valueOf(LocalDateTime.now())),
    )
    val response: Seq[Response] = transactionsToPerform.map(processor.performTransaction).filter(_.isDefined).map(_.get)
    response.map(_.accepted) mustBe Seq(true)
  }
}
