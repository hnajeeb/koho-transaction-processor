package com.koho.transactionprocessor.process

import java.sql.Timestamp
import java.time.LocalDateTime

import com.koho.transactionprocessor.model.Transaction
import com.koho.transactionprocessor.testutil.UnitTest

import scala.collection.mutable

class VelocityValidatorTest extends UnitTest{

  "Daily limit of 5000" should "not be exceeded in a single transaction" in {
    val transaction = Transaction(1234, 1111, 5001, Timestamp.valueOf(LocalDateTime.now()))
    val isValid = VelocityValidator.validate(transaction,new mutable.HashMap[String, Transaction]())
    isValid mustBe false
  }

  it should "not be exceeded in multiple(2) transactions" in {
    val dateTime = LocalDateTime.of(2020,6,28, 9,35,23,12)
    val transactions = new mutable.HashMap[String, Transaction]()
    transactions.put("1234_1111", Transaction(1234, 1111, 3000, Timestamp.valueOf(dateTime)))

    val transaction = Transaction(1235, 1111, 3000, Timestamp.valueOf(dateTime.plusHours(12)))
    val isValid = VelocityValidator.validate(transaction, transactions)

    isValid mustBe false
  }

  "Weekly limit of 20000" should "not be exceeded" in {
    val dateTime = LocalDateTime.of(2020,7,28, 9,35,23,12)
    val transactions = new mutable.HashMap[String, Transaction]()
    Seq(
      (1233, 1111, 5000, Timestamp.valueOf(dateTime.minusDays(1))),
      (1234, 1111, 5000, Timestamp.valueOf(dateTime)),
      (1235, 1111, 5000, Timestamp.valueOf(dateTime.plusDays(1))),
      (1236, 1111, 5000, Timestamp.valueOf(dateTime.plusDays(2))),
    ).map(item =>
      transactions.put(s"${item._1}_${item._2}", Transaction(item._1, item._2, item._3, item._4)))
    val transaction = Transaction(1237, 1111, 1, Timestamp.valueOf(dateTime.plusDays(3)))
    val isValid = VelocityValidator.validate(transaction, transactions)
    isValid mustBe false
  }
}
