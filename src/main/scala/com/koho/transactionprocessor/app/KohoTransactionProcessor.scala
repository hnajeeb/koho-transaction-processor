package com.koho.transactionprocessor.app

import com.koho.transactionprocessor.config.Config
import com.koho.transactionprocessor.controller.Controller
import com.koho.transactionprocessor.util.Logger

object KohoTransactionProcessor extends App with Logger {

  val parser = new scopt.OptionParser[Config]("scopt") {
    head("scopt", "3.7.1")
    opt[String]("inputFileName").required().action { (x, c) =>
      c.copy(inputFile = x)
    } text ("InputFileName is a String property")
    opt[Boolean]("writeToFile").required().action { (x, c) =>
      c.copy(writeToFile = x)
    } text ("WriteToFile is a Boolean property")
    opt[String]("outputFileName").action { (x, c) =>
      c.copy(outputFile = Some(x))
    } text ("OutputFileName is a String property")
  }
  parser.parse(args, Config()) map {
    config =>
      config.validateConfig
      val controller = new Controller(config)
      controller.execute()
  }

}
