package com.koho.transactionprocessor.model

case class Response(
                   id: Long,
                   customer_id: Long,
                   accepted: Boolean
                   )
