package com.koho.transactionprocessor.model

import java.sql.Timestamp

case class Transaction(
                      id: Long,
                      customer_id: Long,
                      load_amount: Double,
                      time: Timestamp,
                      ){
  lazy val date = time.toLocalDateTime.toLocalDate
  //lazy val load_amount: Double = amountString.stripPrefix("$").toDouble
}
