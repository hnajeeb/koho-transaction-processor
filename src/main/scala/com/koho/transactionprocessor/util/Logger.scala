package com.koho.transactionprocessor.util
import org.slf4j.LoggerFactory

trait Logger {
  val logger = LoggerFactory.getLogger(getClass)
}
