package com.koho.transactionprocessor.io

import java.io.File

import scala.io.Source
import io.circe._, io.circe.parser._

object FileReader {

  def readFromFile(filename: String): Seq[String] = {
    Source.fromFile(new File(filename)).getLines().toSeq
  }
}
