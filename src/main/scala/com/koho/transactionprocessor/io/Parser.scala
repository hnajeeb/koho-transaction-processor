package com.koho.transactionprocessor.io

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter._


import com.koho.transactionprocessor.model.Transaction
import com.koho.transactionprocessor.util.Logger
import io.circe._
import io.circe.parser._


object Parser extends Logger {

  def parseJsonToTransactions(lines: Seq[String]) = {
     lines.map {
      line =>
        parse(line) match {
          case Left(failure) =>
            logger.info(s"Invalid JSON: $line")
            None
          case Right(json) =>
            val cursor = json.hcursor
            val id: Decoder.Result[Long] = cursor.downField("id").as[Long]
            val customerid: Decoder.Result[Long] = cursor.downField("customer_id").as[Long]
            val amount: Decoder.Result[String] = cursor.downField("load_amount").as[String]
            val timestamp: Decoder.Result[String] = cursor.downField("time").as[String]
            (id, customerid, amount, timestamp) match{
              case (Right(v1),Right(v2),Right(v3), Right(v4)) =>
                val load_amount = v3.stripPrefix("$").toDouble
                val time = Timestamp.valueOf(LocalDateTime.parse(v4, ISO_DATE_TIME))
                Some(Transaction(v1, v2, load_amount, time))
              case _ => None
            }
        }
    }.filter(_.isDefined).map(_.get)
  }


}
