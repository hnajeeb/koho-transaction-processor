package com.koho.transactionprocessor.io

import java.io.{File, FileWriter}

import com.koho.transactionprocessor.model.Response
import com.koho.transactionprocessor.util.Logger
import io.circe.generic.auto._
import io.circe.syntax._

object ResultWriter extends Logger {

  def writeToConsole(responses: Seq[Response]): Unit = {
    responses.foreach(response =>
      println(response.asJson.noSpaces)
    )
  }

  def writeToFile(filename: String, responses: Seq[Response]):Unit = {
    val file = new File(filename)
    val fileWriter = new FileWriter(file)
    responses.foreach(response => fileWriter.write(response.asJson.noSpaces + "\n"))
    fileWriter.flush()
    fileWriter.close()
    logger.info(s"Output written to file $filename")
  }
}
