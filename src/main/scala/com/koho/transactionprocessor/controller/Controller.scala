package com.koho.transactionprocessor.controller

import com.koho.transactionprocessor.config.Config
import com.koho.transactionprocessor.io.{FileReader, Parser, ResultWriter}
import com.koho.transactionprocessor.process.Processor
import com.koho.transactionprocessor.util.Logger

class Controller(config: Config) extends Logger{

  def execute(): Unit = {
    logger.info("Starting Koho Transaction Processing")
    val lines = FileReader.readFromFile(config.inputFile)
    val transactionsToProcess = Parser.parseJsonToTransactions(lines)
    val processor = new Processor()
    val response = transactionsToProcess.map(processor.performTransaction).filter(_.isDefined).map(_.get)
    config.writeToFile match {
      case true =>
        ResultWriter.writeToFile(config.outputFile.getOrElse("Output.txt"), response)
      case false =>
        ResultWriter.writeToConsole(response)
    }
    logger.info("Process Completed")
  }






}
