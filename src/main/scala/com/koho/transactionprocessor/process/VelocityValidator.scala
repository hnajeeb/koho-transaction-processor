package com.koho.transactionprocessor.process

import java.sql.Timestamp
import java.util.Calendar

import com.koho.transactionprocessor.model.Transaction

import scala.collection.mutable

object VelocityValidator {

  def validate(transaction: Transaction, transactions: mutable.HashMap[String, Transaction]): Boolean = {
    val weeklyUsedLimit = getWeeklyLimit(transaction, transactions)
    val dailyUsedLimit = getDailyUsedLimit(transaction, transactions)
    if ((dailyUsedLimit._2 + transaction.load_amount) > 5000.00
      || dailyUsedLimit._1 >= 3 || (weeklyUsedLimit + transaction.load_amount) > 20000.00) return false
    true
  }

  private def getDailyUsedLimit(transaction: Transaction, transactions: mutable.HashMap[String, Transaction]): (Int, Double) = {
    val loadAmountSeq = transactions.values.toSeq.filter { item =>
      item.customer_id == transaction.customer_id &&
        item.date == transaction.date
    }
      .map(_.load_amount)
    (loadAmountSeq.size, loadAmountSeq.sum)
  }

  private def getWeeklyLimit(transaction: Transaction, transactions: mutable.HashMap[String, Transaction]): Double = {
    transactions.values.toSeq.filter(item => item.customer_id == transaction.customer_id &&
      isSameWeek(item.time, transaction.time)
    ).map(_.load_amount).sum
  }

  private def isSameWeek(oldTransactionTime: Timestamp, transactionTime: Timestamp): Boolean = {
    val (transactionWeek, transactionYear) = getWeekNumberAndYear(transactionTime)
    val (oldTransactionWeek, oldTransactionYear) = getWeekNumberAndYear(oldTransactionTime)
    (transactionWeek == oldTransactionWeek) && (transactionYear == oldTransactionYear)
  }

  private def getWeekNumberAndYear(time: Timestamp): (Int, Int) = {
    val calendar = Calendar.getInstance()
    calendar.setFirstDayOfWeek(Calendar.MONDAY)
    calendar.setTime(time)
    val weekNumber = calendar.get(Calendar.WEEK_OF_YEAR)
    val year = calendar.get(Calendar.YEAR)
    (weekNumber, year)
  }
}
