package com.koho.transactionprocessor.process

import java.util

import com.koho.transactionprocessor.model.{Response, Transaction}
import com.koho.transactionprocessor.util.Logger

import scala.collection.mutable

class Processor extends Logger {
  private val transactions = new mutable.HashMap[String,Transaction]

  def performTransaction(transaction: Transaction): Option[Response] = {
    val isValid = VelocityValidator.validate(transaction, transactions)
    isValid match {
      case true =>
        if(transactions.contains(s"${transaction.id}_${transaction.customer_id}")) {
          logger.info(s"Transaction ${transaction.id} for customer ${transaction.customer_id} was ignored")
          None
        }
        else {
          transactions.put(s"${transaction.id}_${transaction.customer_id}", transaction)
          Some(Response(transaction.id, transaction.customer_id, isValid))
        }
      case _ =>
        Some(Response(transaction.id, transaction.customer_id, isValid))
    }
  }

}
