package com.koho.transactionprocessor.config

case class Config(
                   inputFile: String = "input.txt",
                   writeToFile: Boolean = true,
                   outputFile: Option[String] = None
                 ){
  def validateConfig() = {
    if (writeToFile && outputFile.isEmpty) throw new IllegalArgumentException("missing parameter outputfilename")
  }
}
