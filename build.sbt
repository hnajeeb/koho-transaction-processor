import Dependencies._

name := "KohoTransactionProcessor"
version := "0.1"
scalaVersion := "2.13.3"
libraryDependencies ++= Seq(scalatest, scopt, scalaLogging) ++ circe
